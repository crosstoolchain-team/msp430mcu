#!/bin/sh
#
# Copyright (c) 2011, Peter A. Bigot, licensed under New BSD (see COPYING)
# This file is part of msp430mcu (http://sourceforge.net/projects/mspgcc/)

# Generate the msp430mcu files
MSP430MCU_ROOT=${MSP430MCU_ROOT:-${MSP430_ROOT:-/msp430}/msp430mcu}
export MSP430MCU_ROOT
cd ${MSP430MCU_ROOT}
PYTHONPATH=${MSP430MCU_ROOT}/lib/python${PYTHONPATH+:${PYTHONPATH}}
export PYTHONPATH
scripts/characterize.sh
python scripts/checkdev.py > analysis/checkdev.out
python scripts/genspecs.py
scripts/genlink.sh
python scripts/genmem.py
scripts/periphs.sh
