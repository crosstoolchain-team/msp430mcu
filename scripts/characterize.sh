#!/bin/sh
#
# Copyright (c) 2011, Peter A. Bigot, licensed under New BSD (see COPYING)
# This file is part of msp430mcu (http://sourceforge.net/projects/mspgcc/)
#
# Analyze TI headers to characterize chips categories based on the
# legacy header they reference.

MSP430MCU_ROOT=${MSP430MCU_ROOT:-${MSP430_ROOT:-/msp430}/msp430mcu}
UPSTREAM=${MSP430MCU_ROOT}/upstream
ANALYSIS=${MSP430MCU_ROOT}/analysis
SCRIPTS=${MSP430MCU_ROOT}/scripts

mkdir -p ${ANALYSIS} || exit 1
cd ${ANALYSIS}

# Identify the chips
cat ${UPSTREAM}/devices.csv \
  | tr '[\015]' '[ ]' \
  | sed \
     -e '/^\s*#/d' \
     -e '/^\s*$/d' \
  | cut -d, -f1 \
  > chip.ids

# Identify which headers are marked as legacy.
( cd ${UPSTREAM} && ls *430x*.h ) > legacy.lst

# Identify the non-legacy headers
( cd ${UPSTREAM} && ls *430*.h | sed -e '/^[a-z]*430x/d' -e '/^msp430.h$/d' ) > nonlegacy.lst

# Non-legacy headers have a comment block at the top that is the sole
# difference between them and the chips that are considered in that
# equivalence class.  Remove the block.
mkdir tmphdr$$
cp -p ${UPSTREAM}/*430*.h tmphdr$$
for f in `cat ${ANALYSIS}/legacy.lst` ; do
  uf=${UPSTREAM}/${f}
  if ( sed -n 34p ${uf} | grep -q 'Legacy Header' ) ; then
    sed -e '33,38d' ${uf} > tmphdr$$/${f}
  fi
done

# Calculate checksums for all headers
(cd tmphdr$$ && md5sum *430*.h) | sort > ${ANALYSIS}/filesum.lst
rm -rf tmphdr$$

# Create a table where column 0 identifies the legacy header and subsequent
# columns list the chips that are equivalent to it
cat legacy.lst \
 | while read file ; do
    sha=$(grep ' '"${file}"'$' filesum.lst | cut -d' ' -f1)
    echo -n "${file}	"
    grep "^${sha}" filesum.lst \
      | awk '{print $2;}' \
      | grep -v ${file} \
      | paste -d' ' -s -
   done \
 > chip-equiv.txt

# Generate the chip flags data
python ${SCRIPTS}/chipflags.py
