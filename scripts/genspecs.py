# Copyright (c) 2011, Peter A. Bigot, licensed under New BSD (see COPYING)
# This file is part of msp430mcu (http://sourceforge.net/projects/mspgcc/)
#
# Read the chipflags.csv file and generate individual msp430_spec.spec
# files that will be referenced by the gcc spec file to determine
# device-specific default flags without requiring compiler
# modifications.

import sys
import csv
import os

msp430_root = os.environ.get('MSP430_ROOT', '/msp430')
msp430mcu_root = os.environ.get('MSP430MCU_ROOT', os.path.join(msp430_root, 'msp430mcu'))
analysis_dir = os.path.join(msp430mcu_root, 'analysis')
upstream_dir = os.path.join(msp430mcu_root, 'upstream')

def analysis_path (filename):
    global analysis_dir
    return os.path.join(analysis_dir, filename)

def upstream_path (filename):
    global upstream_dir
    return os.path.join(upstream_dir, filename)

reader = csv.reader(file(analysis_path('chipflags.csv')))

fields = reader.next()


cpu_spec = []
mpy_spec = []
ivcnt_spec = []

sections = { 'cpu': cpu_spec,
             'mpy': mpy_spec,
             'ivcnt': ivcnt_spec }

remap_from = { }
for line in file(analysis_path('../scripts/legacy.map')):
    tokens = line[:line.find('#')].split()
    if not tokens:
        continue
    key = tokens.pop(0)
    if tokens:
        value = tokens.pop(0)
    else:
        value = key.replace('430x', '430f').lower()
    remap_from[key] = value
remap_to = { }
for (k, v) in remap_from.items():
    while v in remap_from:
        v = remap_from[v]
    remap_to.setdefault(v, []).append(k)

for row in reader:
    args = dict(zip(('mcu', 'xmcu', 'cpu', 'mpy', 'intr'), row))

    mcu_selector = 'mmcu=%(mcu)s|mmcu=%(mcu)s_*' % args
    aux_mcu = remap_to.get(args['mcu'])
    if aux_mcu is not None:
        mcu_selector += '|%s' % ('|'.join([ 'mmcu=%s' % (_xmcu,) for _xmcu in aux_mcu ]),)
    args['mcusel'] = mcu_selector
    # Define mcu-specific values for CPU, hardware multiplier, and
    # interrupt vector length.
    cpu_spec.append('%(mcusel)s:-mcpu=%(cpu)s' % args)
    mpy_spec.append('%(mcusel)s:-mmpy=%(mpy)s' % args)
    ivcnt_spec.append('%(mcusel)s:-mivcnt=%(intr)s' % args)

cpu_spec.append(':-mcpu=430')
cpu_spec[:] = [ '%{!mcpu=*:%{', '; '.join(cpu_spec), '}}', '%{mcpu=*}' ]

mpy_spec.append(':-mmpy=none')
mpy_spec[:] = [ '%{!mmpy=*:%{', '; '.join(mpy_spec), '}}', '%{mmpy=*}' ]

ivcnt_spec.append(':-mivcnt=16')
ivcnt_spec[:] = [ '%{!mivcnt=*:%{', '; '.join(ivcnt_spec), '}}', '%{mivcnt=*}' ]

outf = file(analysis_path('msp430mcu.spec'), 'w')

for (block, spec) in sections.items():
    outf.write("*msp430_%s: %s\n\n" % (block, " ".join(spec)))
