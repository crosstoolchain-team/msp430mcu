#!/bin/sh
#
# Copyright (c) 2011, Peter A. Bigot, licensed under New BSD (see COPYING)
# This file is part of msp430mcu (http://sourceforge.net/projects/mspgcc/)
#
# Generate linker scripts that define addresses for all peripheral
# registers supported on a particular chip.

MSP430MCU_ROOT=${MSP430MCU_ROOT:-${MSP430_ROOT:-/msp430}/msp430mcu}
UPSTREAM=${MSP430MCU_ROOT}/upstream
ANALYSIS=${MSP430MCU_ROOT}/analysis
SCRIPTS=${MSP430MCU_ROOT}/scripts

FILES=$(cat ${ANALYSIS}/nonlegacy.lst)
GLD=${ANALYSIS}/ldscripts
rm -rf ${GLD}
mkdir -p ${GLD}
for f in ${FILES} ; do
  bn=$(basename $f .h)
  mkdir -p ${GLD}/${bn}
  (
    cat ${UPSTREAM}/${f} \
    | sed -f ${SCRIPTS}/genlink.sed \
    | cpp \
    | grep '^%regaddr' \
    | sed -e 's@^%regaddr\.\([^ ]*\) *@__@' \
          -e 's@0 +1@1@' \
	  -e 's@2 +1@3@' \
	  -e 's@4 +1@5@' \
	  -e 's@6 +1@7@' \
	  -e 's@8 +1@9@' \
	  -e 's@A +1@B@' \
	  -e 's@C +1@D@' \
	  -e 's@E +1@F@' \
  ) > ${GLD}/${bn}/periph.x
done
