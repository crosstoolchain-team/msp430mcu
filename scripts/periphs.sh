#!/bin/sh
#
# Extract the peripheral presence identifiers from each header,
# generating lists of all peripherals available on each MCU, and all
# MCUs supported by each peripheral.

MSP430MCU_ROOT=${MSP430MCU_ROOT:-${MSP430_ROOT:-/msp430}/msp430mcu}
UPSTREAM=${MSP430MCU_ROOT}/upstream
ANALYSIS=${MSP430MCU_ROOT}/analysis
SCRIPTS=${MSP430MCU_ROOT}/scripts

cd ${ANALYSIS}

rm -rf peripherals
mkdir -p peripherals
cat nonlegacy.lst \
 | while read MCU ; do
   mcu=$(basename ${MCU} .h)
   grep 'define\s*__MSP430_HAS_' ../upstream/${MCU} \
    | awk '{print $2;}' \
    | sort \
    | uniq \
    | sed -e 's@^__MSP430_HAS_\(.*\)__$@\1@' \
    > peripherals/${mcu}.periph
done

cat peripherals/*.periph \
  | sort \
  | uniq \
  > peripherals.lst

cat peripherals.lst \
  | while read PERIPH ; do
    grep -l "^${PERIPH}\$" peripherals/*.periph \
    | sort \
    | sed -e 's@^.*/\([^/]*\).periph@\1@' \
    > peripherals/${PERIPH}.mcus
done
