#!/bin/sh
#
# Copyright (c) 2011, Peter A. Bigot, licensed under New BSD (see COPYING)
# This file is part of msp430mcu (http://sourceforge.net/projects/mspgcc/)

PREFIX=${1:-/msp430/install/dev}
MSP430MCU_ROOT=${MSP430MCU_ROOT:-${MSP430_ROOT:-/msp430}/msp430mcu}
UPSTREAM=${MSP430MCU_ROOT}/upstream
ANALYSIS=${MSP430MCU_ROOT}/analysis
SCRIPTS=${MSP430MCU_ROOT}/scripts
VERSION=`cat ${MSP430MCU_ROOT}/.version`
UPSTREAM_VERSION=`cat ${MSP430MCU_ROOT}/upstream/.version`

BINPATH=${PREFIX}/bin
INCPATH=${PREFIX}/msp430/include
LIBPATH=${PREFIX}/msp430/lib

mkdir -p ${INCPATH} ${LIBPATH}

# Upstream headers
install -p -m 0644 ${UPSTREAM}/*.h ${INCPATH}

# Local override headers
install -p -m 0644 ${MSP430MCU_ROOT}/include/*.h ${INCPATH}

# Override msp430.h to accommodate legacy MSPGCC MCU identifiers
install -p -m 0644 ${ANALYSIS}/msp430.h ${INCPATH}

# MCU-specific data for GCC driver program
install -p -m 0644 ${ANALYSIS}/msp430mcu.spec ${LIBPATH}

# Install MCU-specific memory and periph maps
cp -pr ${ANALYSIS}/ldscripts ${LIBPATH}
chmod -R og+rX ${LIBPATH}/ldscripts

# Install utility that tells where everything got installed
cat bin/msp430mcu-config.in \
| sed \
    -e 's!@PREFIX@!'"${PREFIX}"'!g' \
    -e 's!@SCRIPTPATH@!'"${LIBPATH}/ldscripts"'!g' \
    -e 's!@INCPATH@!'"${INCPATH}"'!g' \
    -e 's!@VERSION@!'"${VERSION}"'!g' \
    -e 's!@UPSTREAM_VERSION@!'"${UPSTREAM_VERSION}"'!g' \
> ${BINPATH}/msp430mcu-config \
&& chmod 0755 ${BINPATH}/msp430mcu-config
