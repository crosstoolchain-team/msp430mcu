# Copyright (c) 2011, Peter A. Bigot, licensed under New BSD (see COPYING)
# This file is part of msp430mcu (http://sourceforge.net/projects/mspgcc/)
#
# Generate material that can be cut and pasted into the legacy mspgcc4
# sources to add support for a specific chip.  For emergency use only:
# effort is now directed towards the uniarch solution.

import sys
import csv
import re
import os.path

analysis_dir = '/msp430/msp430mcu/analysis'
upstream_dir = '/msp430/msp430mcu/upstream'

def analysis_path (filename):
    global analysis_dir
    return os.path.join(analysis_dir, filename)

def upstream_path (filename):
    global upstream_dir
    return os.path.join(upstream_dir, filename)

# Part-number regex.
pn_re = re.compile('''
^(?P<processorFamily>[a-z]*)
(?P<platform>430)
(?P<memoryType>[cefgpl])
(?P<endEquipmentOptimized>[gew]?)
(?P<generation>[0-9])
(?P<series>[0-9])
(?P<family>[0-9]+)
(?P<variant>.*)
$''', re.VERBOSE + re.IGNORECASE)

# Alternative regex.  This exists solely because TI decided to denote
# a bluetooth-optimized device (msp430bt5190) without any regard for
# consistency with their existing part number schem.

alt_pn_re = re.compile('''
^(?P<processorFamily>[a-z]*)
(?P<platform>430)
(?P<special>[^0-9]+)
(?P<generation>[0-9])
(?P<series>[0-9])
(?P<family>[0-9]+)
(?P<variant>.*)
$''', re.VERBOSE + re.IGNORECASE)

PN_FIELDS = ( 'processorFamily', 'platform', 'memoryType', 'endEquipmentOptimized',
              'generation', 'series', 'family', 'variant' )

# The following maps from TI generic names to the ISA type from legacy GCC.
# Entries marked "GENERALIZED" correspond to generics for which no existing
# chip was supported in legacy GCC; they have been assigned ISA values
# based solely on inference from the chip number, and have not been validated.

gisa_map = {'msp430xw42x': '42', # GENERALIZED
            'msp430x32x': '32',
            'msp430x417': '41',
            'msp430xe42xa': '42', # GENERALIZED
            'msp430xe42x2': '42', # GENERALIZED
            'msp430x42x': '42',
            'msp430x41x': '41',
            'msp430x471x3': '471', # GENERALIZED
            'msp430x14x': '14',
            'msp430x16x': '16',
            'cc430x613x': '54',
            'msp430xg47x': '47', # GENERALIZED
            'msp430x47x': '47', # GENERALIZED
            'msp430x23x': '23',
            'msp430xe42x': '42', # GENERALIZED
            'msp430x471x7': '471',
            'msp430x471x6': '471',
            'msp430x12x': '12',
            'msp430x43x': '43',
            'msp430x23x0': '23',
            'msp430x551x': '54', # GENERALIZED, PROBABLY WRONG
            'msp430x20x2': '20',
            'msp430x20x3': '20',
            'msp430x20x1': '20',
            'msp430x47x3': '47',
            'msp430x21x2': '22',
            'msp430x21x1': '21',
            'msp430x47x4': '47',
            'msp430x12x2': '12',
            'msp430xg46x': '46', # GENERALIZED
            'msp430x42x0': '42',
            'msp430x24x1': '24',
            'msp430x11x2': '110',
            'msp430xg43x': '43', # GENERALIZED
            'msp430x54xa': '54', # GENERALIZED
            'msp430x13x': '13',
            'msp430x15x': '15',
            'msp430x43x1': '43',
            'msp430x26x': '26',
            'msp430x552x': '54', # GENERALIZED, PROBABLY WRONG
            'msp430x44x1': '44', # GENERALIZED
            'msp430x22x2': '22',
            'msp430x22x4': '22',
            'msp430x415': '41',
            'cc430x612x': '54',
            'msp430x31x': '31',
            'msp430x46x': '46', # GENERALIZED
            'msp430x54x': '54',
            'msp430x461x1': '46', # GENERALIZED
            'msp430x11x': '11',
            'msp430x44x': '44',
            'msp430xg42x0': '42', # GENERALIZED
            'msp430x24x': '24',
            'msp430x14x1': '14',
            'cc430x513x': '54',
            'msp430x13x1': '13',
            'msp430x11x1': '110',
            'msp430x09x': '54', # GENERALIZED
            'msp430x41x2': '41', # GENERALIZED
            'msp430x241x': '241',
            'msp430x33x': '33',
            'msp430x415': '41', # GENERALIZED
            'msp430x417': '41', # GENERALIZED
            }

generics = []
chip_equiv = { }
for line in file(analysis_path('chip-equiv.txt')).readlines():
    # Strip off .h extension
    chips = [ _fn[:-2] for _fn in line.split() ]
    generic = chips.pop(0)
    suffix = generic[generic.find('430x')+4:]
    if 0 > suffix.find('x'):
        # Not really a generic; multiple chips with identical headers
        # e.g. msp430f415 msp430f417.  @TODO@ Generalize this, currently
        # handles only the one case.
        chips = [ generic.replace('x', 'f') ]
    generics.append(generic)
    isa = gisa_map.get(generic)
    for specific in chips:
        assert not specific in chip_equiv, "%s already defined" % (specific,)
        m = pn_re.match(specific)
        if m is not None:
            pndict = m.groupdict()
            pndict['memoryType'] = 'x'
            #pndict['endEquipmentOptimized'] = ''
            #pndict['variant'] = ''
            xmcu = ''.join([ pndict.get(_field) for _field in PN_FIELDS ])
            xisa = gisa_map.get(xmcu)
            if isa is None:
                isa = xisa
            elif (xisa is not None) and isa != xisa:
                print 'Generic %s isa %s but specific %s isa %s' % (generic, isa, specific, xisa)
            chip_equiv.setdefault(xmcu, generic)
            assert chip_equiv[xmcu] == generic, 'Mismatch %s %s %s' % (xmcu, chip_equiv[xmcu], generic)
        chip_equiv[specific] = generic
    if isa is None:
        print 'No ISA for %s' % (generic,)

reader = csv.reader(open('chipflags.csv'))
reader.next()
include_data = { }
for row in reader:
    mcu = row.pop(0)
    include_data[mcu] = row

# Device Name,STACKSIZE,CPU_TYPE,CPU_Bugs,MPY_TYPE,RAMStart,RAMEnd,INFOStart,INFOSize,INFOEnd,INFOA,INFOB,INFOC,INFOD,FStart,FEnd,FStart2,FEnd2,INTStart,INTEnd
reader = csv.reader(file(upstream_path('devices.csv')))
reader.next()

xmcus = []
ld_makefile_objlist = []
ld_makefile_rules = []
msp430all_checks = []
gas_tc_msp430_c = []
gcc_msp430_c = []

isa_vecs = { }

for row in reader:
    mcu = row.pop(0)            # Device Name
    row.pop(0)                  # STACKSIZE
    cpu = row.pop(0)            # CPU_TYPE
    bugs = row.pop(0)           # CPU_Bugs
    mpy = row.pop(0)            # MPY_TYPE

    if not mcu in include_data:
        print 'No include file for %s' % (mcu,)
        continue

    pndict = None
    m = pn_re.match(mcu)
    if m is not None:
        pndict = m.groupdict()
    else:
        m = alt_pn_re.match(mcu)
        if m is not None:
            pndict = m.groupdict()
            special = pndict.pop('special')
            if 'bt' == special:
                # Bluetooth-optimized device
                pass
            elif 2 == len(special):
                pndict['memoryType'] = special[0]
                pndict['endEquipmentOptimized'] = special[1]
    if pndict is None:
        print 'No match to "%s"' % (mcu,)
        continue

    pndict['memoryType'] = 'x'
    xmcu = ''.join([ pndict.get(_field, '') for _field in PN_FIELDS ])
    print '%s -> %s' % (mcu, xmcu)
    xmcus.append(xmcu)

    gmcu = chip_equiv.get(mcu, chip_equiv.get(xmcu, xmcu))
    isa = gisa_map.get(gmcu)
    if isa is None:
        if pndict['generation'] in ('5', '6'):
            isa = '54'
        else:
            isa = '%s%s' % (pndict['generation'], pndict['series'])
        #print 'ISA for %s generic %s set to %s' % (mcu, gmcu, isa)

    (rams, rame, infos, infosz, infoe, infoas, infobs, infocs, infods,
     flashs, flashe, flash2s, flash2e, ints, inte) = [ int(_x, 16) for _x in row ]
    ramz = 1 + rame - rams
    flashz = 0x10000 - flashs + 1 + flash2e - flash2s
    infoz = 1 + infoe - infos
    isa_vecs.setdefault(isa, set()).add(ints)

    ld_makefile_objlist.append("\te%s.o \\" % (xmcu,))
    ld_makefile_rules.append('''e%(xmcu)s.c: $(srcdir)/emulparams/msp430all.sh \\
  $(ELF_GEN_DEPS) $(srcdir)/scripttempl/elf32msp430.sc \\
  ${GEN_DEPENDS}
	${GENSCRIPTS} %(xmcu)s "$(tdir_%(xmcu)s)" msp430all''' % locals())

    msp430all_checks.append('''
if [ "${MSP430_NAME}" = "%(xmcu)s" ] ; then
ARCH=msp:%(isa)s
ROM_START=0x%(flashs)x
ROM_SIZE=0x%(flashz)x
INFO_START=0x%(infos)x
INFO_SIZE=%(infoz)u
BOOT_START=0x1000
BOOT_SIZE=2048
RAM_START=0x%(rams)x
RAM_SIZE=%(ramz)u
VECTORS_START=0x%(ints)x
fi''' % locals())

    gas_tc_msp430_c.append('  {"%(xmcu)s",  MSP430_ISA_%(isa)s,  bfd_mach_msp%(isa)s},' % locals())

    has_mpy = int(not not include_data[mcu][1])

    gcc_msp430_c.append("\t{\"%(xmcu)s\",  MSP430_ISA_%(isa)s, %(has_mpy)d}," % locals())

print "# xmcu list:"
print " ".join(xmcus)

print "# ld/Makefile.am object files:"
print "\n".join(ld_makefile_objlist)

print "\n\n# ld/Makefile.am rules:"
print "\n".join(ld_makefile_rules)

print "\n\n# msp430all.sh blocks:"
print "\n".join(msp430all_checks)

print "\n\n# gas/config/tc-msp430.c defn:"
print "\n".join(gas_tc_msp430_c)

print "\n\n# gcc/config/msp430.c defn:"
print "\n".join(gcc_msp430_c)

ints = set()
for (k, s) in isa_vecs.items():
    ints.update(s)

for s in ints:
    for (k, s2) in isa_vecs.items():
        if s in s2:
            print 'MSP430_ISA_%s: %x' % (k, s)
