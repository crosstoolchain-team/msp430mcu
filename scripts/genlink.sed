# Copyright (c) 2011, Peter A. Bigot, licensed under New BSD (see COPYING)
# This file is part of msp430mcu (http://sourceforge.net/projects/mspgcc/)
#
# Ignore the IAR intrinsic file
/#include "in430.h"/d

# Replace the iomacros with inline definitions we can use to recognize
# register address assignments
/#include <iomacros.h>/c\
#define sfrb(x,x_) %regaddr.b x = x_\
#define sfrw(x,x_) %regaddr.w x = x_\
#define sfra(x,x_) %regaddr.a x = x_\
#define const_sfrb(x,x_) %regaddr.cb x = x_\
#define const_sfrw(x,x_) %regaddr.cw x = x_\
#define const_sfra(x,x_) %regaddr.ca x = x_
