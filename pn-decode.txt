Updated MSP430-line part number decoding can be found at:

http://focus.ti.com/en/graphics/mcu/mcuovw/MSP430_Part_Number_Decoder_2011.png

Device type "P" is no longer documented, but the former reference had marked
it as "OTP", presumably for "one-time program".
